# Digsig

Digital signatures for pdf documents for gemalto smart cards on Ubuntu 20.04 and 22.04
In an updated ubuntu system the only requierment is firefox browser.
The following may work for other distributions of Linux 64 bit and not for 32 bit systems.
The installation procedure was applied to a system with the following properties :

`firefox version: 84.0.2`

`kernel version : 5.8.0-34-generic`

`$ apt-cache policy libssl-dev`
```
libssl-dev:
  Installed: 1.1.1f-1ubuntu2.1
  Candidate: 1.1.1f-1ubuntu2.1
  Version table:
 *** 1.1.1f-1ubuntu2.1 100
        100 /var/lib/dpkg/status
     1.1.1f-1ubuntu2 500
        500 http://archive.ubuntu.com/ubuntu focal/main amd64 Packages
```
Although, a similar procedure was succeeded in OpenSuse.

key words
=========
- luxtrust
- ClassicClient for Linux
- Gemalto Smart card
- autofirma

Credits
========
[here](https://it.auth.gr/sites/default/files/downloads/%CE%A5%CF%80%CE%BF%CE%B3%CF%81%CE%B1%CF%86%CE%AE_%CE%B5%CE%B3%CE%B3%CF%81%CE%AC%CF%86%CE%BF%CF%85%20_Linux.pdf)

Directions
==========

**1.  libgclib.so** You need to instal the shared library libgclib.so. 
First check if you have installed pcscd and libusb. If not provide

`$sudo apt-get install libusb-0.1-4`

`$sudo apt-get install pcscd`

`$sudo apt-get install pcsc-tools`

If you do not have libssl1, you will need it.

`$wget http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.0g-2ubuntu4_amd64.deb`

`$sudo dpkg -i ./libssl1.1_1.1.0g-2ubuntu4_amd64.deb`

Also, pixbuf:

`$sudo apt-get install libgdk-pixbuf2.0-0`

- libusb is a C library that provides generic access to USB devices
- pcscd is  a Smart Card Daemon

Then you need to download a suitable file from Luxtrust. The following is **proprietary software** so you have to agree with EULA.
Initially download (51 MB) [`https://www.luxtrust.lu/downloads/middleware/LuxTrust_Middleware_1.5.0_Ubuntu_64bit.tar.gz`](https://www.luxtrust.com/sites/default/files/downloads/middleware/LuxTrust_Middleware_1.5.0_Ubuntu_64bit.tar.gz)
or find a new from https://www.luxtrust.com/en/middleware

There are two files in the previous tar file.
-  (i). Gemalto middleware contains the driver for the Gemalto smart card. This is what we need.
- (ii). luxtrust_middleware (is a certification authority that issues and manages digital certificates)

So, after you unzip the previous file, give
 `$sudo dpkg -i Gemalto_Middleware_Ubuntu_64bit_x.x.x-xxx.xx.deb`

Although, many vendors provide proprietary drivers for their smart cards. Some smart cards suppported by open source linux drivers can be found [here](https://github.com/OpenSC/OpenSC/wiki/Supported-hardware-%28smart-cards-and-USB-tokens%29)

If your installaton for some reasons failed :(, you have to periodically check for a new driver in 
[`https://www.luxtrust.lu/en/simple/225`](https://www.luxtrust.lu/en/simple/225)

Now you must have the file `/usr/lib/ClassicClient/libgclib.so`.  

For **Ubuntu 20.04** we suggest:

**2.  autofirma**
Since we need a client in order to sign pdf documents, we need one more program to install. In windows ms machines they use acrobat reader. We shall use autofirma. Autofirma needs java 8, so if you don't have java you have to install:

`$sudo apt-get install openjdk-8-jre`

Install [autofirma](https://firmaelectronica.gob.es/Home/Descargas.html) or you can download it from this repository 
- `$wget https://gitlab.com/draz_/digsig/-/raw/master/autofirma_1.6.5-2_all.deb`
Then, 
- `$sudo dpkg -i autofirma_1.6.5-2_all.deb`
It is an open source client for signing pdf documents created by Spanish goverment with licence GPL 2+ y EUPL 1.1.
If you don't have a debian based distribution e.g. OpenSuse, then you may download the rpm version of autofirma.

For **Ubuntu 22.04** we recommend:

**2. okular**

`sudo apt install okular`

Continue for **20.04** and **22.04** :

**3. Firefox settings**
- write `about:preferences#privacy` in firefox 
- choose `Security` -> `Certificates `-> `Security Devices` and `Load`
- Choose a name for your device, for instance gemalto
- In Module filename add the following path : `/usr/lib/ClassicClient/libgclib.so`

Stop here if you have ubuntu 22.04.
-----------------------------------
That's it. Now, you must be able to digitally signe pdf documents with okular. If for some reason your card is 
not "communicate" with the usb port try:

`sudo systemctl start pcscd`

For Ubuntu 20.04 continue as below:
-----------------------------------

- Plugin you smartcard and go to View Certificates and a window will pop up asking for the pin of your card. Write the pin.
- Now, write `about:profiles` in firefox and delete `default` profile
- finally rename `default-release` to `default` and `reboot your machine`
(see [`section 2`](https://it.auth.gr/sites/default/files/downloads/%CE%A5%CF%80%CE%BF%CE%B3%CF%81%CE%B1%CF%86%CE%AE_%CE%B5%CE%B3%CE%B3%CF%81%CE%AC%CF%86%CE%BF%CF%85%20_Linux.pdf) in greek)



**4. Parametrise autofirma** Start autofirma (`$autofirma`). After you import the pin of your card, you need to upload the pdf for signing, and then you will be asked to add your signature, see [here](https://gitlab.com/draz_/digsig/-/blob/master/2021-02-09_22-00.png)
Write 
`Digitally signed by $$SUBJECTCN$$ Date:$$SIGNDATE=dd/MM/yyyy HH:mm:ss +02''00''$$`

Instead of +02 add the correct one for your country.
This is only for the first time, autofirma remembers the last changes.
For the signing steps with autofirma see the pictures [Pictures](https://gitlab.com/draz_/digsig/-/tree/master/Pictures) directory in this repository.
Note that, to check the details of a digital signature imported in a pdf file, you may need [master pdf editor](https://code-industry.net/free-pdf-editor/).
Autofirma is recommended by European Union (https://joinup.ec.europa.eu/collection/spain-center-technology-transfer/solution/firma-digital-signature-client)

A comment for Ubuntu 20.04 (LTS)
================================
If in your Ubuntu you use the dark windows colors, choose Light (from settings--> appearence).

Possibly drawbacks
===================
When you start autofirma you may get the following error
```
Exception in thread "main" java.lang.NoClassDefFoundError: Could not initialize class java.awt.Toolkit
	at java.desktop/java.awt.Color.<clinit>(Color.java:275)
	at es.gob.afirma.standalone.LookAndFeelManager.<clinit>(LookAndFeelManager.java:60)
	at es.gob.afirma.standalone.SimpleAfirma.main(SimpleAfirma.java:549)
```

You can easily fix this by changing the version of java.
```
$java --version 
openjdk 11.0.11 2021-04-20
OpenJDK Runtime Environment (build 11.0.11+9-Ubuntu-0ubuntu2.20.04)
OpenJDK 64-Bit Server VM (build 11.0.11+9-Ubuntu-0ubuntu2.20.04, mixed mode)
```

See

`$update-java-alternatives -l`

if you get `version 1.8.*`, then provide

`$sudo update-alternatives --config java`
and select `java-8-openjdk`
If you have not installed java-8, [you have to install it](https://itectec.com/ubuntu/ubuntu-downgrading-java-11-to-java-8-duplicate/). For instance Ubuntu 20.04.03 has java-11 by default, so 
autofirma will fail. If you have applications that need the latest version of openjdk, then revert to the previous, by using again,
`$sudo update-alternatives --config java`

Grades
======
This concerns a specific platofrm (https://gitlab.com/universis/).

1. `$wget https://gitlab.com/universis/digital-signing-native-app/-/raw/master/Native%20App/artifacts/LinuxGradesDigitalSigning`
2. `$chmod u+x LinuxGradesDigitalSigning`
3. `$./LinuxGradesDigitalSigning`

and you are done.

you can make an `alias` in .bashrc, for instance add

`alias grades="cd /path/to/LinuxGradesDigitalSigning && ./LinuxGradesDigitalSigning"`
and then `$source .bashrc`
So, now you can start the app by giving `$grades `

Useful readings
=================
[1] [Smart Cards in Linux](https://archive.fosdem.org/2018/schedule/event/smartcards_in_linux/attachments/slides/2265/export/events/attachments/smartcards_in_linux/slides/2265/smart_cards_slides.pdf)

[2] [Autofirma videos (in spanish)](https://firmaelectronica.gob.es/Home/Empresas/Empresas-Video-Firma?dId=E_1)

[3] [autofirma for ios](https://play.google.com/store/apps/details?id=es.gob.afirma)

[4] [autofirma for android](https://play.google.com/store/apps/details?id=es.gob.afirma)

[5] [source code of autofirma](https://github.com/ctt-gob-es/clienteafirma)

Questions
==========
Pls provide feedback. You can open a new [issue](https://gitlab.com/draz_/digsig/-/issues) for questions. 

